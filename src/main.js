const { invoke } = window.__TAURI__.tauri;

let SONG = {};
let FILENAME;

window.addEventListener("DOMContentLoaded", async () => {
  await window.__TAURI__.event.listen('open', async (_) => {
    FILENAME = await window.__TAURI__.dialog.open({
      filters: [{
        name: 'Song',
        extensions: ['json']
      }]
    });
    if (FILENAME !== undefined) {
      load(FILENAME).then((_) => draw());
    }
    // event.event is the event name (useful if you want to use a single callback fn for multiple event types)
    // event.payload is the payload object
  })
  await window.__TAURI__.event.listen('new', async (_) => {
    FILENAME = await window.__TAURI__.dialog.save({
      filters: [{
        name: 'Song',
        extensions: ['json']
      }]
    });
    if (FILENAME !== undefined) {
      SONG = {
        title: "Untitled",
        authors: [],
        tags: [],
        lyrics: [],
        refrain: [],
        chords: []
      };
      draw();
    }
    // event.event is the event name (useful if you want to use a single callback fn for multiple event types)
    // event.payload is the payload object
  })
  await window.__TAURI__.event.listen('save', async (_) => {
    await invoke('save_file', { filename: FILENAME, content: JSON.stringify(SONG, null, 4) });
  })
  document.getElementById("lyrics_edit").style.display = "none";
});

async function polyfilled_prompt(p) {
  let res = prompt(p);
  if (res === null) {
    let prompt_div = document.getElementById("html_prompt");
    document.getElementById("prompt_text").innerText = p;
    prompt_div.style.display = "block";
    return await new Promise((res, _) => {
      document.getElementById("prompt_confirm").onclick = () => {
        prompt_div.style.display = "none";
        res(document.getElementById("prompt_data").value);
      };
    })
  } else {
    return res;
  }
}


function print_chord(chord, prefix) {
  let chord_el = document.createElement("span");
  let chord_text = document.createElement("span");
  chord_text.innerText = chord
  let chord_prefix = document.createElement("span");
  chord_prefix.innerText = prefix
  chord_prefix.style.visibility = "hidden";
  chord_prefix.ariaHidden = true;
  chord_el.appendChild(chord_prefix);
  chord_el.appendChild(chord_text);
  return chord_el;
}
async function load(path) {
  SONG = await invoke('open_file', { filename: path }).then(JSON.parse);
}
function draw() {
  document.getElementsByTagName("title")[0].innerText = `${SONG.title} - Songbook Editor`;
  document.getElementsByTagName("h1")[0].innerText = SONG.title;
  let chord_map = [];
  for (const l of SONG.lyrics) {
    chord_map.push([]);
    for (const _ of l) {
      chord_map[chord_map.length - 1].push([]);
    }
  }
  SONG.chords.forEach((chord, i) => {
    let el = print_chord(chord.chord, SONG.lyrics[chord.stanza][chord.row].substring(0, chord.position));
    el.onclick = (_) => {
      SONG.chords.splice(i, 1);
      draw();
    }
    chord_map[chord.stanza][chord.row].push(el);
  })
  let lyrics = document.getElementById("lyrics");
  lyrics.innerHTML = "";
  SONG.lyrics.forEach((stanza, i) => {
    let s = document.createElement("p");
    lyrics.appendChild(s);
    stanza.forEach((row, j) => {
      if (chord_map[i][j].length > 0) {
        let chords = document.createElement("div");
        s.appendChild(chords);
        let current_offset = 0;
        for (let c of chord_map[i][j]) {
          c.style.marginLeft = `${current_offset * -1}px`;
          chords.appendChild(c);
          current_offset = c.offsetWidth;
        }
        chords.classList.add("chord");
      }
      let r = document.createElement("div");
      for (let k = 0; k < row.length; k++) {
        let char_span = document.createElement("span");
        char_span.innerText = row[k];
        char_span.classList.add("editable");
        char_span.onclick = async () => {
          SONG.chords.push({
            stanza: i,
            row: j,
            position: k,
            chord: await polyfilled_prompt("Chord")
          });
          draw();
        }
        r.appendChild(char_span);
      }
      s.appendChild(r);
    });
    if (SONG.refrain[i]) {
      s.classList.add("refrain");
    }
  });
  let tag_list = document.getElementById("tags");
  for (const tag of SONG.tags) {
    //let tag_element = create_tag(conf, tag);
    //tag_list.appendChild(tag_element);
  }
}

function edit_lyrics() {
  document.getElementById("lyrics_edit").style.display = "initial";
  let area = document.getElementById("raw_lyrics");
  let intermediate = [];
  for (const stanza of SONG.lyrics || []) {
    intermediate.push(stanza.join("\n"));
  }
  area.value = intermediate.join("\n\n");
}

function update_lyrics() {
  document.getElementById("lyrics_edit").style.display = "none";
  let area = document.getElementById("raw_lyrics");
  SONG.lyrics = [];
  SONG.refrain = [];
  for (const stanza of area.value.split("\n\n")) {
    SONG.lyrics.push(stanza.split("\n"));
    SONG.refrain.push(false);
  }
  draw();
}
