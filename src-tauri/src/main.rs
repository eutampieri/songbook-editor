#![cfg_attr(
    all(not(debug_assertions), target_os = "windows"),
    windows_subsystem = "windows"
)]

use tauri::{CustomMenuItem, Menu, Submenu};

// Learn more about Tauri commands at https://tauri.app/v1/guides/features/command
#[tauri::command]
fn open_file(filename: String) -> String {
    std::fs::read_to_string(filename).unwrap_or_else(|_| "{}".to_owned())
}
#[tauri::command]
fn save_file(filename: String, content: String) {
    std::fs::write(filename, content).unwrap_or_else(|_| ())
}

fn main() {
    // here `"quit".to_string()` defines the menu item id, and the second parameter is the menu item label.
    let quit = CustomMenuItem::new("quit".to_string(), "Quit");
    let close = CustomMenuItem::new("close".to_string(), "Close");
    let open = CustomMenuItem::new("open".to_string(), "Open");
    let save = CustomMenuItem::new("save".to_string(), "Save");
    let new = CustomMenuItem::new("new".to_string(), "New");
    let submenu = Submenu::new(
        "File",
        Menu::new()
            .add_item(new)
            .add_item(open)
            .add_item(save)
            .add_item(close)
            .add_item(quit),
    );
    let menu = Menu::new().add_submenu(submenu);

    tauri::Builder::default()
        .menu(menu)
        .on_menu_event(|event| match event.menu_item_id() {
            "quit" => {
                std::process::exit(0);
            }
            _ => {
                event.window().emit(&event.menu_item_id(), "").unwrap();
            }
        })
        .invoke_handler(tauri::generate_handler![open_file, save_file])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
